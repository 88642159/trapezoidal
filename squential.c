#include <stdio.h>

double f(double x) {
	return x*x;
}

void main() {
	
	double a=2.00;
	double b=5.00;
	int n=3;
	double h;
	double x_i, approx;
	int i;
	
	printf("Enter a, b, n number: ");
	scanf("%lf %lf %d", &a, &b, &n);
	/* scanf("%f", &b); */
	/* scanf("%d", &n); */

	h = (b-a)/n;
	approx = (f(a) + f(b))/2.0;
	for (i = 0; i <= n-1; i++) {
		x_i = a + i*h;
		approx += f(x_i);
	}
	approx = h*approx;

	printf("Intergral Answer: %lf\n", approx);
}
